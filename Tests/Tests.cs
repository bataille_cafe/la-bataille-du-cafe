using System;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class Tests
    {
        [Fact]
        public void TestMods()
        {
            var v_shifts = new List<(int, int)>();
            var v_cardinals = new List<(int, int)>()
            {
                (0,-1),
                (0,1),
                (-1,0),
                (1,0)
            };
            for (int card = 0; card < 4; card++)
            {
                //Formulas stating from which side we are shifting(north,south,east,west)
                v_shifts.Add((card % 2 * (card - 2), (card + 1) % 2 * (card - 1)));
            }
            foreach (var card in v_cardinals)
            {
                Assert.Contains(card, v_shifts);
            }
        }
    }
}
