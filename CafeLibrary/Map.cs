﻿using System;
using System.Collections.Generic;

namespace CafeLibrary
{
    public sealed class Map
    {
        // SINGLETON PATTERN
        public static readonly Map instance = new Map();

        // ATTRIBUTES
        public const int MapSize = 21;
        public Tile[,] ActualMap { get; }
        List<Parcel> Parcels { get; set; }

        // CONSTRUCTOR
        private Map()
        {
            ActualMap = new Tile[MapSize, MapSize];
            Parcels = new List<Parcel>();
        }

        /// <summary>
        /// Set Borders end on the map
        /// </summary>
        public static void SetDefaultBorders()
        {
            for (int i = 0; i < MapSize; i++)
            {
                instance.ActualMap[i, 0] = new Border(false, true, i, 0);
                instance.ActualMap[i, MapSize - 1] = new Border(false, true, i, MapSize - 1);
                instance.ActualMap[0, i] = new Border(true, true, 0, i);
                instance.ActualMap[MapSize - 1, i] = new Border(true, true, MapSize - 1, i);
            }
        }


        /// <summary>
        /// Display map on Console
        /// </summary>
        public static void DisplayMap()
        {
            Console.WriteLine($"Mapsize = {Map.MapSize}");
            for (int i = 0; i < MapSize; i++)
            {
                for (int j = 0; j < MapSize; j++)
                    Console.Write(instance.ActualMap[i, j].DisplayTile());
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Displays Parcel Id on each of the tiles
        /// </summary>
        public static void DisplayParcels()
        {
            for (int i = 1; i < MapSize; i+=2)
            {
                for (int j = 1; j < MapSize; j += 2)
                    if (Map.instance.ActualMap[i, j] is PlantableTile)
                    {
                        PlantableTile tile = (PlantableTile)Map.instance.ActualMap[i, j];
                        Console.Write(tile.DisplayParcelNumber() + "-");
                    }
                    else
                        Console.Write("n");
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Function which gets calculate parcels from map and stores it in Parcels List of Map instance
        /// </summary>
        public static void InitParcels()
        {
            byte parcelCount = 0;
            Parcel currentParcel = new Parcel(parcelCount);
            instance.Parcels.Add(currentParcel);
            
            for (byte i = 1; i < MapSize; i += 2) {
                for (byte j = 1; j < MapSize; j += 2)
                {
                    bool northOk = false, westOk = false;
                    try
                    {
                        if (Map.instance.ActualMap[i, j] is PlantableTile && (Map.instance.ActualMap[i - 2, j] is PlantableTile || Map.instance.ActualMap[i, j - 2] is PlantableTile))
                        {
                            try
                            {
                                PlantableTile tile = (PlantableTile)Map.instance.ActualMap[i, j];

                                Border northBorder = (Border)Map.instance.ActualMap[i - 1, j];
                                Border westBorder = (Border)Map.instance.ActualMap[i, j - 1];

                                if (!westBorder.IsHere)
                                {
                                    PlantableTile westTile = (PlantableTile)Map.instance.ActualMap[i, j - 2];
                                    tile.SetParcelNumber(westTile.ParcelNumber);
                                    tile.Parcel = westTile.Parcel;
                                    currentParcel.AddTile(tile);
                                    westOk = true;
                                }
                                if (!northBorder.IsHere)
                                {
                                    PlantableTile northTile = (PlantableTile)Map.instance.ActualMap[i - 2, j];
                                    tile.SetParcelNumber(northTile.ParcelNumber);
                                    tile.Parcel = northTile.Parcel;
                                    currentParcel.AddTile(tile);
                                    northOk = true;
                                }

                                if (northOk && westOk)
                                {
                                    PlantableTile westTile = (PlantableTile)Map.instance.ActualMap[i, j - 2];
                                    westTile.SetParcelNumber(tile.ParcelNumber);
                                    westTile.Parcel = tile.Parcel;
                                    currentParcel.AddTile(westTile);
                                }

                                if (!westOk && !northOk)
                                {
                                    tile.SetParcelNumber(parcelCount);
                                    tile.Parcel = currentParcel;
                                    parcelCount++;
                                    currentParcel = new Parcel(parcelCount);
                                    instance.Parcels.Add(currentParcel);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                        else if (Map.instance.ActualMap[i, j] is PlantableTile)
                        {
                            PlantableTile tile = (PlantableTile)Map.instance.ActualMap[i, j];
                            tile.SetParcelNumber(parcelCount);
                            tile.Parcel = currentParcel;
                            currentParcel.AddTile(tile);
                            parcelCount++;
                            currentParcel = new Parcel(parcelCount);
                            instance.Parcels.Add(currentParcel);
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        PlantableTile tile = (PlantableTile)Map.instance.ActualMap[i, j];
                        tile.SetParcelNumber(parcelCount);
                        tile.Parcel = currentParcel;
                        currentParcel.AddTile(tile);
                        parcelCount++;
                        currentParcel = new Parcel(parcelCount);
                        instance.Parcels.Add(currentParcel);
                    }
                }
            }
        }
    }
}
