﻿using System;
using System.Text.RegularExpressions;

namespace CafeLibrary
{
    public static class Utils
    {
        /// <summary>
        /// Exit Program with a code, 0 if everything went well
        /// </summary>
        /// <param name="code"></param>
        /// <param name="socket"></param>
        public static void ExitGame(byte code, MySocket socket)
        {
            socket.CloseSocket();
            System.Environment.Exit(code);
        }

        /// <summary>
        /// Verify if given seed can be played and add it to the map
        /// </summary>
        /// <param name="socketClient"></param>
        public static void SendPlayAnalyzeResponse(MySocket socketClient, byte x, byte y)
        {
            Console.WriteLine($"PLAYING ON : {x}:{y}");
            socketClient.SendData(x, y);

            string msg;
            msg = socketClient.ReceiveData(4);

            switch (msg)
            {
                case "INVA":
                    Console.WriteLine("SAISIE INVALIDE");
                    break;
                case "VALI":
                    if (Map.instance.ActualMap[x * 2 + 1, y * 2 + 1] is PlantableTile)
                    {
                        PlantableTile tile = (PlantableTile)Map.instance.ActualMap[x * 2 + 1, y * 2 + 1];
                        tile.Owner = Owner.Us;
                    }
                    else
                        Console.WriteLine("THERE WAS AN ERROR WHILE ATTEMPTING TO MODIFY MAP");
                    break;
                default:
                    ExitGame(0, socketClient);
                    break;
            }
        }

        /// <summary>
        /// Get Server seed placement for the current turn and add it on map
        /// </summary>
        /// <param name="socketClient"></param>
        public static void GetServerPlay(MySocket socketClient)
        {
            string msg;
            msg = socketClient.ReceiveData(4);

            if (Regex.IsMatch(msg, @"^B:\d{2}$"))
            {
                string msgSplitted = msg.Split(':')[1];
                byte x = (byte)char.GetNumericValue(msgSplitted[0]);
                byte y = (byte)char.GetNumericValue(msgSplitted[1]);
                if(Map.instance.ActualMap[x * 2 + 1, y * 2 + 1] is PlantableTile)
                {
                    PlantableTile tile = (PlantableTile)Map.instance.ActualMap[x * 2 + 1, y * 2 + 1];
                    tile.Owner = Owner.Enemy;
                    AI.instance.SetLastPlayedCoordinates(x, y);
                }
            }
            else
            {
                if (msg.Trim().Equals("FINI"))
                    Utils.GetResults(socketClient);
                else
                    GetResults(socketClient);
            }
                
        }

        /// <summary>
        /// Function that asks server if party's still going on or if it has to end
        /// </summary>
        public static void GetPlayPossibility(MySocket socketClient)
        {
            string msg;
            msg = socketClient.ReceiveData(4);

            switch (msg)
            {
                case "ENCO":
                    return;
                case "FINI":
                    GetResults(socketClient);
                    ExitGame(0,socketClient);
                    break;
                default:
                    ExitGame(2, socketClient);
                    break;
            }
        }

        /// <summary>
        /// Displays Results of curent game and exit it
        /// </summary>
        /// <param name="socketClient"></param>
        public static void GetResults(MySocket socketClient)
        {
            string msg = socketClient.ReceiveData(10);
            string[] msgSplitted = msg.Split(':');
            Console.WriteLine($"FINAL SCORE : AI {msgSplitted[1]} - SERVER {msgSplitted[2]}");
            ExitGame(0,socketClient);
        }
    }
}
