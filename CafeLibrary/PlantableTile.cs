﻿using System;

namespace CafeLibrary
{
    public class PlantableTile : Tile
    {
        // ATTRIBUTES
        public Owner Owner;
        public byte ParcelNumber;
        public Parcel Parcel;

        // CONSTRUCTOR
        public PlantableTile(int _X, int _Y)
        {
            X = _X;
            Y = _Y;
            Owner = Owner.Nobody;
        }

        // SETTERS
        public void SetParcelNumber(byte parcelNumber)
        {
            ParcelNumber = parcelNumber;
        }

        /// <summary>
        /// Display Tile on Console
        /// </summary>
        /// <returns></returns>
        public override string DisplayTile()
        {
            switch (Owner)
            {
                case Owner.Nobody:
                    return "N";
                case Owner.Enemy:
                    return "E";
                case Owner.Us:
                    return "U";
            }
            throw new ArgumentException("Tile doesn't have Owner property");
        }

        /// <summary>
        /// Display the Parcel Number on Console
        /// </summary>
        /// <returns></returns>
        public byte DisplayParcelNumber()
        {
            return ParcelNumber;
        }
    }
}
