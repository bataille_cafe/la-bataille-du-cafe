﻿using System;

namespace CafeLibrary
{
    public class UnplantableTile : Tile
    {
        // ATTRIBUTES
        GroundType GroundType;

        // CONSTRUCTOR
        public UnplantableTile(int _X, int _Y, GroundType _groundType)
        {
            X = _X;
            Y = _Y;
            GroundType = _groundType;
        }

        /// <summary>
        /// Display Tile on Console
        /// </summary>
        /// <returns></returns>
        public override string DisplayTile()
        {
            switch (GroundType)
            {
                case GroundType.Sea:
                    return "S";
                case GroundType.Forest:
                    return "F";
            }
            throw new ArgumentException();
        }
    }
}
