﻿namespace CafeLibrary
{
    public static class Frame
    {
        /* Fonction qui traduit la trame
         * Paramètres : string trame est une chaîne de caractères séparés par des '|' et des ':'
         *              taille_carte correspond à la taille de la carte
         * Fonctionnement : Découpe la chaîne et à chaque '|', création d'une nouvelle ligne
         *                  Puis découpe les lignes en éléments selon les ':'
         *                  On obtient donc autant de lignes que de '|' et autant d'éléments dans une ligne que de ':' dans la ligne
         * Sortie : Tableau d'entiers de 10 par 10 représentant les cases par des entiers (à traiter plus tard)
         */
        public static int[,] TranslateFrame(string trame, int taille_carte)
        {
            int[,] carte = new int[taille_carte, taille_carte];

            // Split the trame in a tab when string encounters a '|'
            string[] trame_splitted_lines = new string[10];
            trame_splitted_lines = trame.Split('|');

            //Split each of the string in the temporary tab trame_splitted_lines
            for (int i = 0; i < taille_carte; i++)
            {
                string[] trame_current_line = trame_splitted_lines[i].Split(':');
                for (int j = 0; j < taille_carte; j++)
                {
                    carte[i, j] = int.Parse(trame_current_line[j]);
                }
            }
            return carte;
        }

        /// <summary>
        /// Create a Tile Map from double-dimensionned array
        /// </summary>
        /// <param name="trame"></param>
        public static void ConstructMapFromFrame(int[,] trame)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    int value = trame[i, j];
                    bool hasEast = false, hasSouth = false, hasWest = false, hasNorth = false;

                    if (value >= 64)
                    {
                        value -= 64;
                        Map.instance.ActualMap[2 * i + 1, 2 * j + 1] = new UnplantableTile(2 * i + 1, 2 * j + 1, GroundType.Sea);
                    }
                    else if (value >= 32)
                    {
                        value -= 32;
                        Map.instance.ActualMap[2 * i + 1, 2 * j + 1] = new UnplantableTile(2 * i + 1, 2 * j + 1, GroundType.Forest);
                    }
                    else
                    {
                        PlantableTile tile = new PlantableTile(2 * i + 1, 2 * j + 1);
                        tile.SetParcelNumber(0);
                        Map.instance.ActualMap[2 * i + 1, 2 * j + 1] = tile;
                    }
                        

                    if (value >= 8)
                    {
                        value -= 8;
                        Map.instance.ActualMap[2 * i + 1, 2 * j + 2] = new Border(false, true, 2 * i + 1, 2 * j + 2);
                        hasEast = true;
                    }
                    else
                        Map.instance.ActualMap[2 * i + 1, 2 * j + 2] = new Border(false, false, 2 * i + 1, 2 * j + 2);

                    if (value >= 4)
                    {
                        value -= 4;
                        Map.instance.ActualMap[2 * i + 2, 2 * j + 1] = new Border(true, true, 2 * i + 2, 2 * j + 1);
                        hasSouth = true;
                    }
                    else
                        Map.instance.ActualMap[2 * i + 2, 2 * j + 1] = new Border(true, false, 2 * i + 2, 2 * j + 1);

                    if (value >= 2)
                    {
                        value -= 2;
                        Map.instance.ActualMap[2 * i + 1, 2 * j] = new Border(false, true, 2 * i + 1, 2 * j);
                        hasWest = true;
                    }
                    else
                        Map.instance.ActualMap[2 * i + 1, 2 * j] = new Border(false, false, 2 * i + 1, 2 * j);

                    if (value >= 1)
                    {
                        value -= 1;
                        Map.instance.ActualMap[2 * i, 2 * j + 1] = new Border(true, true, 2 * i, 2 * j + 1);
                        hasNorth = true;
                    }
                    else
                        Map.instance.ActualMap[2 * i, 2 * j + 1] = new Border(true, false, 2 * i, 2 * j + 1);

                    if (hasEast)
                        Map.instance.ActualMap[2 * i + 2, 2 * j + 2] = new Border(true, true, 2 * i + 2, 2 * j + 2, true);
                    else
                        Map.instance.ActualMap[2 * i + 2, 2 * j + 2] = new Border(true, false, 2 * i + 2, 2 * j + 2);

                    if (hasSouth)
                        Map.instance.ActualMap[2 * i + 2, 2 * j] = new Border(true, true, 2 * i + 2, 2 * j, true);
                    else
                        Map.instance.ActualMap[2 * i + 2, 2 * j] = new Border(true, false, 2 * i + 2, 2 * j);

                    if (hasWest)
                        Map.instance.ActualMap[2 * i, 2 * j] = new Border(true, true, 2 * i, 2 * j, true);
                    else
                        Map.instance.ActualMap[2 * i, 2 * j] = new Border(true, false, 2 * i, 2 * j);

                    if (hasNorth)
                        Map.instance.ActualMap[2 * i, 2 * j + 2] = new Border(true, true, 2 * i, 2 * j + 2, true);
                    else
                        Map.instance.ActualMap[2 * i, 2 * j + 2] = new Border(true, false, 2 * i, 2 * j + 2);
                }
            }
        }
    }
}
