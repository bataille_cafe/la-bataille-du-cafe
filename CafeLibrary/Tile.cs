﻿namespace CafeLibrary
{
    public abstract class Tile
    {
        public int X { get; set; }
        public int Y { get; set; }

        public abstract string DisplayTile();
    }
}
