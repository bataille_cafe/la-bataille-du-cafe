﻿namespace CafeLibrary
{
    public class Border : Tile
    {
        // ATTRIBUTES
        private bool IsHorizontal;
        public bool IsHere;
        private bool IsCross;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_IsHorizontal"></param>
        /// <param name="_IsHere"></param>
        /// <param name="_X"></param>
        /// <param name="_Y"></param>
        /// <param name="isCross"></param>
        public Border(bool _IsHorizontal, bool _IsHere, int _X, int _Y, bool isCross=false)
        {
            IsHorizontal = _IsHorizontal;
            IsHere = _IsHere;
            X = _X;
            Y = _Y;
            IsCross = isCross;
        }

        /// <summary>
        /// Display Tile on Console
        /// </summary>
        /// <returns></returns>
        public override string DisplayTile()
        {
            if (IsHere)
            {
                if (IsCross) return "+";
                if (IsHorizontal) return "-";
                else return "|";
            }
            else
                return " ";
        }
    }
}
