﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CafeLibrary
{
    public sealed class AI
    {
        public static readonly AI instance = new AI();

        // ATTRIBUTES
        public byte LastSeedPlayedX { get; set; }
        public byte LastSeedPlayedY { get; set; }
        private List<byte[]> playableTiles = new List<byte[]>();

        private AI()
        {
            LastSeedPlayedX = 255;
            LastSeedPlayedY = 255;
        }

        // SETTER
        public void SetLastPlayedCoordinates(byte _X, byte _Y)
        {
            LastSeedPlayedX = _X;
            LastSeedPlayedY = _Y;
        }

        /// <summary>
        /// GET coordinates where next seed can be placed
        /// </summary>
        /// <param name="_X"></param>
        /// <param name="_Y"></param>
        public byte[] GetPossibleTilePlays(byte _X, byte _Y)
        {
            byte actualParcelNumber = 255;

            if ((_X >= 0 && _X < 10) && (_Y >= 0 && _Y < 10))
            {
                PlantableTile tile = (PlantableTile)Map.instance.ActualMap[_X * 2 + 1, _Y * 2 + 1];
                actualParcelNumber = tile.ParcelNumber;

                // GETTING PLAYABLE TILES ON X AXIS
                for (int i = 1; i < Map.MapSize; i += 2)
                {
                    if (Map.instance.ActualMap[i, _Y * 2 + 1] is PlantableTile)
                    {
                        PlantableTile plantableTile = (PlantableTile)Map.instance.ActualMap[i, _Y * 2 + 1];
                        if (plantableTile.Owner == Owner.Nobody && plantableTile.ParcelNumber != actualParcelNumber)
                        {
                            byte[] coords = new byte[2];
                            coords[0] = (byte)((Map.instance.ActualMap[i, _Y * 2 + 1].X - 1) / 2);
                            coords[1] = _Y;
                            playableTiles.Add(coords);
                        }
                    }
                }

                // GETTING PLAYABLE TILES ON Y AXIS
                for (int i = 1; i < Map.MapSize; i += 2)
                {
                    if (Map.instance.ActualMap[_X * 2 + 1, i] is PlantableTile)
                    {
                        PlantableTile plantableTile = (PlantableTile)Map.instance.ActualMap[_X * 2 + 1, i];
                        if (plantableTile.Owner == Owner.Nobody && plantableTile.ParcelNumber != actualParcelNumber)
                        {
                            byte[] coords = new byte[2];
                            coords[1] = (byte)((Map.instance.ActualMap[_X * 2 + 1, i].Y - 1) / 2);
                            coords[0] = _X;
                            playableTiles.Add(coords);
                        }
                    }
                }

                /* DISPLAYS IA PLAYS POSSIBILITIES, FOR DEBUG PURPOSES
                Console.WriteLine($"LAST PLAYED : {LastSeedPlayedX},{LastSeedPlayedY}");

                foreach(byte[] array in playableTiles)
                {
                    Console.WriteLine($"x{array[0]},y{array[1]}");
                }*/

                return GetBestPossibility();
            }
            else
            {
                byte[] coords = new byte[2];
                coords[0] = 1;
                coords[1] = 1;
                return coords;
            }  
        }

        /// <summary>
        /// Return the best tile where IA can play for best score
        /// </summary>
        /// <returns></returns>
        public byte[] GetBestPossibility()
        {
            if (playableTiles.Count != 0)
            {
                List<byte> scores = new List<byte>();
                List<byte> adjacentScores = new List<byte>();
                List<byte> parcelScores = new List<byte>();

                // GETTING POTENTIAL SCORE FOR EACH PLAYABLE TILES
                foreach(byte[] array in playableTiles)
                {
                    adjacentScores.Add(GetAdjacentScore(array[0], array[1]));
                    parcelScores.Add(GetParcelScores(array[0], array[1])); // A MODIFIER
                }

                // ADDING SCORES PREVIOUSLY OBTAINED BY DIFFERNET METHODS OF SCORING
                for (byte i = 0; i < adjacentScores.Count; i++)
                {
                    scores.Add((byte)(adjacentScores[i] + parcelScores[i]));
                }

                // GETTING BEST POTENTIAL SCORE FROM LIST
                byte maxScore = 0, index = 0, iterateur = 0;
                foreach(byte score in scores)
                {
                    if(score > maxScore)
                    {
                        maxScore = score;
                        index = iterateur;
                    }
                    iterateur++;
                }

                byte[] coords = playableTiles[index];

                playableTiles.Clear();
                return coords;
            }
            // IF CANNOT PLAY
            byte[] coord = new byte[2];
            coord[0] = 1;
            coord[1] = 1;
            Console.WriteLine("NO PLAYABLE TILE DETETCTED");
            return coord;
        }

        /// <summary>
        /// Test adjacent Tiles and get potential score
        /// </summary>
        /// <param name="_X"></param>
        /// <param name="_Y"></param>
        /// <returns></returns>
        public byte GetAdjacentScore(byte _X, byte _Y)
        {
            byte score = 0;

            try
            {
                // NORTH ADJACENT TILE VERIF
                if (Map.instance.ActualMap[_X * 2 - 1, _Y * 2 + 1] is PlantableTile)
                {
                    PlantableTile tile = (PlantableTile)Map.instance.ActualMap[_X * 2 - 1, _Y * 2 + 1];
                    if (tile.Owner == Owner.Us)
                        score++;
                }
                // SOUTH ADJACENT TILE VERIF
                if (Map.instance.ActualMap[_X * 2 + 3, _Y * 2 + 1] is PlantableTile)
                {
                    PlantableTile tile = (PlantableTile)Map.instance.ActualMap[_X * 2 + 3, _Y * 2 + 1];
                    if (tile.Owner == Owner.Us)
                        score++;
                }
                // EAST ADJACENT TILE VERIF
                if (Map.instance.ActualMap[_X * 2 + 1, _Y * 2 + 3] is PlantableTile)
                {
                    PlantableTile tile = (PlantableTile)Map.instance.ActualMap[_X * 2 + 1, _Y * 2 + 3];
                    if (tile.Owner == Owner.Us)
                        score++;
                }
                // WEST ADJACENT TILE VERIF
                if (Map.instance.ActualMap[_X * 2 + 1, _Y * 2 - 1] is PlantableTile)
                {
                    PlantableTile tile = (PlantableTile)Map.instance.ActualMap[_X * 2 + 1, _Y * 2 - 1];
                    if (tile.Owner == Owner.Us)
                        score++;
                }
            }
            catch (IndexOutOfRangeException)
            {}
            return score;
        }

        /// <summary>
        /// Get potential score that could be obtained by playing on this tile,
        /// taking into account our seeds count, the server seeds count and the parcelsize of the parcel.
        /// </summary>
        /// <param name="_X"></param>
        /// <param name="_Y"></param>
        /// <returns></returns>
        public byte GetParcelScores(byte _X, byte _Y)
        {
            if(Map.instance.ActualMap[_X * 2 + 1, _Y * 2 + 1] is PlantableTile)
            {
                byte score;
                byte parcelSize = 0;
                byte ourSeddsNumber = 0;
                byte serverSeedsNumber = 0;

                PlantableTile tile = (PlantableTile)Map.instance.ActualMap[_X * 2 + 1, _Y * 2 + 1];
                byte actualParcelNumber = tile.ParcelNumber;

                for (int i = 1; i < Map.MapSize; i++)
                {
                    for (int j = 1; i < Map.MapSize; i++)
                    {
                        if (Map.instance.ActualMap[i,j] is PlantableTile)
                        {
                            PlantableTile actualTile = (PlantableTile)Map.instance.ActualMap[i, j];
                            if(actualTile.ParcelNumber == actualParcelNumber)
                            {
                                switch (actualTile.Owner)
                                {
                                    case Owner.Nobody:
                                        parcelSize++;
                                        break;
                                    case Owner.Us:
                                        ourSeddsNumber++;
                                        parcelSize++;
                                        break;
                                    case Owner.Enemy:
                                        serverSeedsNumber++;
                                        parcelSize++;
                                        break;
                                }
                            }
                        }
                    }
                }

                if(ourSeddsNumber > serverSeedsNumber)
                    score = (byte)(parcelSize / 2);
                else if(ourSeddsNumber == serverSeedsNumber)
                    score = parcelSize;
                else
                    score = 1;
                return score;
            }
            return 0;
        }
    }
}
