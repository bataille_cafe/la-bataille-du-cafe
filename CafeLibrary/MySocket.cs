﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace CafeLibrary
{
    public class MySocket
    {
        /// <summary>
        /// Attributes
        /// </summary>
        private static Socket m_socket;
        private static IPEndPoint endPoint;

        /// <summary>
        /// Constructor
        /// </summary>
        public MySocket()
        {
            m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        /* Fonction qui crée une Socket et se connecte à l'IP et port en paramètre
         * Paramètres : string IPAddress est une adresse IP serveur
         *              port est le numéro de port de connexion serveur
         * Fonctionnement : Crée une Socket qui tente de se connecter à un serveur (IP et port)
         *                  Affiche des messages de suivi de la tentative de connexion
         * Sortie : Retourne la Socket si réussite, null si échec et quitte
         */
        public void ConnectSocket(string IPaddress, int port)
        {
            try
            {
                endPoint = new IPEndPoint(IPAddress.Parse(IPaddress), port);
                Console.WriteLine("Tentative de connexion à : {0}  port n° {1}", IPaddress, port);
                m_socket.Connect(endPoint);
                Console.WriteLine("Connexion réussie");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Echec de connexion : {0}", ex.ToString());
                Environment.Exit(1);
            }
        }

        /* Fonction qui récupère une trame à partir d'une Socket
         * Paramètres : Socket connectée à un serveur
         * Fonctionnement : Lit les données envoyées et transforme en chaîne de caractères
         * Sortie : Retourne la chaîne transformée
         */
        public string GetTrame()
        {
            string trame;
            byte[] buffer = new byte[512];
            try
            {
                m_socket.Receive(buffer);
                trame = Encoding.ASCII.GetString(buffer);
                return trame;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Erreur lors de la réception de données : {ex}");
                throw ex;
            }
        }

        /// <summary>
        /// Send coordinates of seed played on this turn
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SendData(int x, int y)
        {
            byte[] msg = new byte[4];
            msg = Encoding.ASCII.GetBytes($"A:{x}{y}");
            m_socket.Send(msg);
        }

        /// <summary>
        /// Get next 4 bytes sent by server
        /// </summary>
        /// <returns></returns>
        public string ReceiveData(byte size)
        {
            byte[] buffer = new byte[size];
            m_socket.Receive(buffer);
            Console.WriteLine(Encoding.ASCII.GetString(buffer).Trim());
            return Encoding.ASCII.GetString(buffer).Trim();
        }

        /* Fonction qui ferme la connexion au serveur en fermant la Socket
         * Paramètres : Socket connectée à un serveur
         * Fonctionnement : Vide le contenu et ferme la Socket
         * Sortie : Rien
         */
        public void CloseSocket()
        {
            try
            {
                m_socket.Shutdown(SocketShutdown.Both);
                m_socket.Close();
                Console.WriteLine("Socket correctement fermée");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Erreur lors de la fermeture de la socket : {0}", ex.ToString());
            }
        }
    }
}
