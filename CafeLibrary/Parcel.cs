﻿using System.Collections.Generic;

namespace CafeLibrary
{
    public class Parcel
    {
        // ATTRIBUTES
        public List<PlantableTile> Tiles;
        public byte numParcel;

        // CONSTRUCTOR
        public Parcel(byte _numParcel)
        {
            numParcel = _numParcel;
            Tiles = new List<PlantableTile>();
        }

        /// <summary>
        /// Add a Tile Object to Pacel List
        /// </summary>
        /// <param name="plantableTile"></param>
        public void AddTile(PlantableTile plantableTile)
        {
            Tiles.Add(plantableTile);
        }
    }
}
