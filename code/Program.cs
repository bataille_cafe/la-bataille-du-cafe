﻿using System;
using CafeLibrary;

namespace projet_tutoré
{
    public class Program
    {
        // PROGRAMME PRINCIPAL
        static void Main(string[] args)
        {
            #region Declarations

            string frame;                                       // Contains the frame
            Tile[,] map = new Tile[Map.MapSize, Map.MapSize];
            int port;                                           // Connexion port
            MySocket socketClient;                              // Socket used for connexion

            #endregion

            #region Getting Frame

            socketClient = new MySocket();
            // Verifying if given arguments are OK
            if (args.Length == 3)
            {
                int.TryParse(args[2], out port);
                socketClient.ConnectSocket(args[1], port);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("IP Address and Port must be given as CLI arguments");
                Console.ForegroundColor = ConsoleColor.White;
                System.Environment.Exit(-1);
            }

            // Getting frame
            frame = socketClient.GetTrame();

            #endregion

            #region Initializing Map
            int[,] trame = Frame.TranslateFrame(frame, 10);

            Frame.ConstructMapFromFrame(trame);

            Map.SetDefaultBorders();
            Map.DisplayMap();
            
            Map.InitParcels();
            Map.DisplayParcels();

            #endregion

            // GAME LOOP
            while (true)
            {
                byte[] coords = AI.instance.GetPossibleTilePlays(AI.instance.LastSeedPlayedX, AI.instance.LastSeedPlayedY );

                Utils.SendPlayAnalyzeResponse(socketClient, coords[0], coords[1]);

                Utils.GetServerPlay(socketClient);

                Utils.GetPlayPossibility(socketClient);

                Map.DisplayMap();
            }
        }
    }
}